using System;
using System.Collections;
using System.Collections.Generic;
using TriLibCore;
using UnityEngine;
using TriLibCore.Samples;
using UnityEngine.UI;

public class TestScript : MonoBehaviour
{
    private string ModelURL;
    // private string ModelURL = "https://ricardoreis.net/trilib/demos/sample/TriLibSampleModel.zip";
    public InputField urlInputField;
    
    /// <summary>
    /// Options used in this sample.
    /// </summary>
    protected AssetLoaderOptions AssetLoaderOptions;
    
    /// <summary>
    /// The AssetLoaderFilePicker instance created for this viewer.
    /// </summary>
    protected AssetLoaderFilePicker FilePickerAssetLoader;
    
    /// <summary>
    /// A flag representing if the model is loading or not.
    /// </summary>
    private bool _loading;
    
    /// <summary>
    /// Loaded game object.
    /// </summary>
    public GameObject RootGameObject { get; protected set; }
    
    public void UploadAsset()
    {
        ModelURL = urlInputField.text;
        var assetLoaderOptions = AssetLoader.CreateDefaultLoaderOptions();
        var webRequest = AssetDownloader.CreateWebRequest(ModelURL);
        AssetDownloader.LoadModelFromUri(webRequest, OnLoad, OnMaterialsLoad, OnProgress, OnError, null, assetLoaderOptions);
    }
    
    public void LoadModel()
    {
        if (urlInputField.text == String.Empty)
        {
            LoadModelFromFile();
        }
        else
        {
            UploadAsset();
        }
    }
    
    public void LoadModelFromFile(GameObject wrapperGameObject = null, Action<AssetLoaderContext> onMaterialsLoad = null)
    {
        FilePickerAssetLoader = AssetLoaderFilePicker.Create();
        FilePickerAssetLoader.LoadModelFromFilePickerAsync("Select a File", OnLoad, onMaterialsLoad ?? OnMaterialsLoad, OnProgress, OnBeginLoadModel, OnError, wrapperGameObject ?? gameObject, AssetLoaderOptions);
    }
    
    protected virtual void OnBeginLoadModel(bool hasFiles)
    {
        if (hasFiles)
        {
            if (RootGameObject != null)
            {
                Destroy(RootGameObject);
            }
        }
    }
    private void OnError(IContextualizedError obj)
    {
        Debug.LogError($"An error occurred while loading your Model: {obj.GetInnerException()}");
    }
    
    private void OnProgress(AssetLoaderContext assetLoaderContext, float progress)
    {
        Debug.Log($"Loading Model. Progress: {progress:P}");
    }
    
    private void OnMaterialsLoad(AssetLoaderContext assetLoaderContext)
    {
        Debug.Log("Materials loaded. Model fully loaded.");
    }
    
    private void OnLoad(AssetLoaderContext assetLoaderContext)
    {
        Debug.Log("Model loaded. Loading materials.");
    }
}
